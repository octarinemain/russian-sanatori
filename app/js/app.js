jQuery(document).ready(function () {

    // head-screen-filter
    $('.extended-filter, .exit-active-filter, .btn-exit').on('click', function () {
        $('.extended-search, .filters, main').toggleClass('active');
        $(document).click(function (e) {
            var block = $(".filters");
            if (!block.is(e.target)
                && block.has(e.target).length === 0) {
                $('.extended-search, .filters, main').removeClass('active');
            }
        });
    });

    //Booking show details
    $('.additionally-setting-title').on('click', function () {
        $(this).find('img').toggleClass('active');
        $(this).parent().toggleClass('active');
    });

    //Search
    $('.search-in').on('focus', function () {
        $('.search-go').addClass('active');
        $(this).on('focusout', function () {
            if ($('.search-in').val() === '') {
                $('.search-go').removeClass('active');
            }
        })
    });


    // //Tabs
    // $('.tabgroup > div').hide();
    // $('.tabgroup > div:first-of-type').show();
    // $('.tabs a').click(function (e) {
    //     e.preventDefault();
    //     var $this = $(this),
    //         tabgroup = '#' + $this.parents('.tabs').data('tabgroup'),
    //         others = $this.closest('li').siblings().children('a'),
    //         target = $this.attr('href');
    //     others.removeClass('active');
    //     $this.addClass('active');
    //     $(tabgroup).children('div').hide();
    //     $(target).show();
    // });

    //Google Map
    function initMap() {
        var uluru = {lat: 44.0529965, lng: 42.8550639};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }

    if ($('div').hasClass('map-sanatorium-card')) {
        initMap();
    }

    //Review
    $(".reviews-full p").each(function () {
        var text = $(this).text();
        $(this).parent().find('input').val(text);
    });

    $(".reviews-full p").html(function (index, currentText) {
        var text = currentText;
        if ($(this).text().length <= 514) {
            return currentText
        } else {
            return currentText.substr(0, 514) + "<a href='#' class='more-text-review'>Еще</a>";
        }
    });

    function reviewH() {
        $('.more-text-review').on('click', function (e) {
            e.preventDefault();
            $(this).parent().parent().find('p').html($(this).parent().parent().find('input').val());
        });
    }

    reviewH();

    //Review PopUp
    $('#add-review').on('click', function (e) {
        e.preventDefault();
        $('main').addClass('active');
        $('.popup-review').addClass('active');
        $('.popup-review-thanks').height($('.popup-review').height());

        $('.exit-active-popup').on('click', function () {
            $('main').removeClass('active');
            $('.popup-review').removeClass('active');
            $('.popup-review-thanks').removeClass('active');
        });

        $('#send-review').on('click', function () {
            $('.popup-review').removeClass('active');
            $('.popup-review-thanks').addClass('active');
        });
    });

    //Review Select
    $('.rating-select').on('click', 'img', function () {
        var rewActive = '<img class="active" src="img/ok-star.svg" alt="">';
        var rewDefault = '<img src="img/not-star.svg" alt="">';
        $(this).prevAll().replaceWith(rewActive);
        $(this).nextAll().replaceWith(rewDefault);
        $(this).replaceWith(rewActive);
    });

    //Slider

    $('.additional-slides').height($('.fancybox').height());

    function moreSlide() {
        var slideLeng = $('.additional-slides a').length;
        var moreHtml = '<div class="more-photo flex-cont align-center justify-c"><img src="img/photo-white.svg" alt=""><span>+15 фото</span></div>';
        var vals;
        if ($(window).outerWidth() >= 1026) {
            vals = 9;
        } else if ($(window).outerWidth() <= 1025 && $(window).outerWidth() >= 721) {
            vals = 8;
        } else if ($(window).outerWidth() <= 720 && $(window).outerWidth() >= 481) {
            vals = 5;
        } else if ($(window).outerWidth() <= 480) {
            vals = 3;
        }

        if (slideLeng >= 10) {
            $('.additional-slides .more-photo').remove();
            $('.additional-slides a').eq(vals).html($('.additional-slides a').eq(vals).html() + moreHtml);
            $('.additional-slides a').eq(vals).addClass('last-slide');
            $('.additional-slides a.fancybox').removeClass('fancybox');
            $('.last-slide').nextAll().addClass('fancybox');
            $('.last-slide').addClass('fancybox');
        } else {
            $('.additional-slides a.fancybox:last-child').html($('.additional-slides a.fancybox:last-child').html() + moreHtml);
            $('.additional-slides a.fancybox:last-child').addClass('last-slide');
        }

        $('.additional-slides a').on('click', function (e) {
            e.preventDefault();
            if (slideLeng >= 10 && !$(this).hasClass('fancybox') && !$(this).hasClass('video')) {
                $('.big-img-slide img').attr('src', $(this).attr('href'));
            }
        });
    }

    moreSlide();

    var paddingSlides;

    if ($(window).outerWidth() >= 721) {
        paddingSlides = 50;
    } else if ($(window).outerWidth() <= 720 && $(window).outerWidth() >= 481) {
        paddingSlides = 30;
    } else if ($(window).outerWidth() <= 480) {
        paddingSlides = 15;
    }

    if ($('a').hasClass('fancybox')) {
        $(".fancybox").fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            nextEffect: 'none',
            prevEffect: 'none',
            padding: [paddingSlides, paddingSlides, paddingSlides, paddingSlides]
        });
    }

    if ($('a').hasClass('video')) {
        $(".video").fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            height: '500',
            helpers: {
                media: {}
            },
            padding: [paddingSlides, paddingSlides, paddingSlides, paddingSlides]
        });
    }

    //Phone
    if ($('input').hasClass('phone-input')) {
        $(".phone-input").mask("+37 (999) 999 99 99");
    }

    //Datepicker
    if ($('input').hasClass('data-start')) {
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $('#data-start').datepicker();
    }

    if ($('input').hasClass('data-end')) {
        $('#data-end').datepicker();
    }

    //Adaptive
    function adaptiv() {
        moreSlide();
        $('.additional-slides').height($('.fancybox').height());
        var tabsLeng = $('.selected-tabs').length - 1;
        //480
        if ($(window).outerWidth() <= 480) {
            $('.data-find .additional-data-filter').clone().prependTo('.additional-data-filter_mob');
            $('.data-find .additional-data-filter').remove();
        } else {
            $('.additional-data-filter').appendTo('.data-find');
        }
        //1025
        if ($(window).outerWidth() <= 1025) {
            $('.additional-data-filter .group.raiting').clone().appendTo('.additional-data-filter_mob');
            $('.additional-data-filter .group.raiting').remove();
            $('.additional-data-filter .group.disease').clone().appendTo('.additional-data-filter_mob');
            $('.additional-data-filter .group.disease').remove();
            $('.header-logo.mob_logo').after($('.header-nav'));
            $('.title-support').appendTo('.support-img');
            //Tab
            $('.selected-tabs').eq(tabsLeng).appendTo('.drop-tabs');
            $('.selected-tabs').eq(tabsLeng - 1).appendTo('.drop-tabs');
            //Booking
            $('.general-info').after($('.booking-info-room .sanatorium-transfer'));
            $('.general-info').after($('.booking-info-room .rating-sticker-wrap'));
        } else {
            $('.group.raiting').appendTo('.additional-data-filter');
            $('.group.disease').appendTo('.additional-data-filter');
            $('.title-support').prependTo('.support-content');
            $('.header-container .header-logo').after($('.header-nav'));
            //Booking
            $('.general-info-booking .rating-sticker-wrap').appendTo('.booking-info-room');
            $('.general-info-booking .sanatorium-transfer').appendTo('.booking-info-room');
        }
        //720
        if ($(window).outerWidth() <= 720) {
            $('.selected-tabs').eq(tabsLeng - 2).appendTo('.drop-tabs');
            $('.selected-tabs').eq(tabsLeng - 3).appendTo('.drop-tabs');
            $('.like-icon img').appendTo('.sanatorium-title');
            //Town height card
            $('.town').height($('.town').width() * 0.7);
            //Booking
            $('.send-info-booking').before($('.price-book-card'));
            $('.send-info-booking').before($('.cancel-booking'));
        } else {
            $('.sanatorium-title img').appendTo('.like-icon');
            //Town height card
            $('.town').height(175);
            //Booking
            $('.your-select-room').after($('.cancel-booking'));
            $('.your-select-room').after($('.price-book-card'));
        }
        //480
        if ($(window).outerWidth() <= 480) {
            $('.selected-tabs').eq(tabsLeng - 4).appendTo('.drop-tabs');
            $('.general-info').after($('.booking-card .geo-position-sanatorium'));
        } else {
            $('.booking-info-room .title-booking-card').after($('.geo-position-sanatorium'));
        }

        //Show towns
        $('.resort-towns-wrap .town').each(function (i, e) {
            if ($(window).outerWidth() >= 1251) {
                if (i <= 5) {
                    $(this).addClass('flex');
                } else {
                    $(this).removeClass('flex');
                }
            } else if ($(window).outerWidth() <= 1250 && $(window).outerWidth() >= 1026) {
                if (i <= 4) {
                    $(this).addClass('flex');
                } else {
                    $(this).removeClass('flex');
                }
            } else if ($(window).outerWidth() <= 1025) {
                if (i <= 3) {
                    $(this).addClass('flex');
                } else {
                    $(this).removeClass('flex');
                }
            }
        });

        //Popular sanatorium
        $('.head-s-popular .sanatorium-card').each(function (i, e) {
            if ($(window).outerWidth() >= 1251) {
                if (i <= 7) {
                    $(this).addClass('active-show');
                } else {
                    $(this).removeClass('active-show');
                }
            } else if ($(window).outerWidth() <= 1250 && $(window).outerWidth() >= 1026) {
                if (i <= 5) {
                    $(this).addClass('active-show');
                } else {
                    $(this).removeClass('active-show');
                }
            } else if ($(window).outerWidth() <= 1025 && $(window).outerWidth() >= 721) {
                if (i <= 3) {
                    $(this).addClass('active-show');
                } else {
                    $(this).removeClass('active-show');
                }
            } else if ($(window).outerWidth() <= 720) {
                if (i <= 1) {
                    $(this).addClass('active-show');
                } else {
                    $(this).removeClass('active-show');
                }
            }
        });

        //Canegories sanatorium
        $('.canegories-p .sanatorium-card').each(function (i, e) {
            if ($(window).outerWidth() >= 1251) {
                if (i <= 15) {
                    $(this).addClass('active-show');
                } else {
                    $(this).removeClass('active-show');
                }
            } else if ($(window).outerWidth() <= 1250 && $(window).outerWidth() >= 1026) {
                if (i <= 11) {
                    $(this).addClass('active-show');
                } else {
                    $(this).removeClass('active-show');
                }
            } else if ($(window).outerWidth() <= 1025 && $(window).outerWidth() >= 721) {
                if (i <= 7) {
                    $(this).addClass('active-show');
                } else {
                    $(this).removeClass('active-show');
                }
            } else if ($(window).outerWidth() <= 720) {
                if (i <= 3) {
                    $(this).addClass('active-show');
                } else {
                    $(this).removeClass('active-show');
                }
            }
        });

        $('.reviews-card').each(function (i, e) {
            if (i <= 2) {
                $(this).addClass('active-show');
            } else {
                $(this).removeClass('active-show');
            }
        });

        //Categories card sanatorium
        $('.categories-card-p .sanatorium-card').each(function (i, e) {
            if ($(window).outerWidth() >= 1251) {
                if (i <= 3) {
                    $(this).addClass('active-show');
                } else {
                    $(this).removeClass('active-show');
                }
            } else if ($(window).outerWidth() <= 1250 && $(window).outerWidth() >= 1026) {
                if (i <= 2) {
                    $(this).addClass('active-show');
                } else {
                    $(this).removeClass('active-show');
                }
            } else if ($(window).outerWidth() <= 1025 && $(window).outerWidth() >= 721) {
                if (i <= 1) {
                    $(this).addClass('active-show');
                } else {
                    $(this).removeClass('active-show');
                }
            } else if ($(window).outerWidth() <= 720) {
                if (i <= 1) {
                    $(this).addClass('active-show');
                } else {
                    $(this).removeClass('active-show');
                }
            }
        });
    }

    adaptiv();

    $('.resort-towns-screen .btn-more').on('click', function () {
        var resizer;
        if ($(window).outerWidth() >= 1251) {
            resizer = 5;
        } else if ($(window).outerWidth() <= 1250 && $(window).outerWidth() >= 1026) {
            resizer = 4;
        } else if ($(window).outerWidth() <= 1025) {
            resizer = 3;
        }
        var lengsAll = $('.town').length;
        var lengsShow = $('.town.flex').length;
        $('.resort-towns-wrap .town').each(function (i, e) {
            if ((i <= (lengsShow + resizer) && (!$(this).hasClass('flex')))) {
                $(this).addClass('flex');
            }
            if ((i == lengsAll - 1) && $(this).hasClass('flex')) {
                lengsAll = $('.town').length;
                lengsShow = $('.town.flex').length;
            }
        });
        if (lengsShow >= lengsAll) {
            $('.resort-towns-screen .more').addClass('hide');
        }
    });

    $('.head-s-popular .btn-more').on('click', function () {
        var resizer;
        if ($(window).outerWidth() >= 1251) {
            resizer = 7
        } else if ($(window).outerWidth() <= 1250 && $(window).outerWidth() >= 1026) {
            resizer = 5
        } else if ($(window).outerWidth() <= 1025 && $(window).outerWidth() >= 721) {
            resizer = 3
        } else if ($(window).outerWidth() <= 720) {
            resizer = 1
        }
        var lengsAll = $('.head-s-popular .sanatorium-card').length;
        var lengsShow = $('.head-s-popular .sanatorium-card.active-show').length;
        $('.head-s-popular .sanatorium-card').each(function (i, e) {
            if ((i <= (lengsShow + resizer) && (!$(this).hasClass('active-show')))) {
                $(this).addClass('active-show');
            }
            if ((i == lengsAll - 1) && $(this).hasClass('active-show')) {
                lengsAll = $('.head-s-popular .sanatorium-card').length;
                lengsShow = $('.head-s-popular .sanatorium-card.active-show').length;
            }
        });
        if (lengsShow >= lengsAll) {
            $('.head-s-popular .more').addClass('hide');
        }
    });

    $('.canegories-p .btn-more').on('click', function () {
        var resizer;
        if ($(window).outerWidth() >= 1251) {
            resizer = 15;
        } else if ($(window).outerWidth() <= 1250 && $(window).outerWidth() >= 1026) {
            resizer = 11;
        } else if ($(window).outerWidth() <= 1025 && $(window).outerWidth() >= 721) {
            resizer = 7;
        } else if ($(window).outerWidth() <= 720) {
            resizer = 3;
        }
        var lengsAll = $('.canegories-p .sanatorium-card').length;
        var lengsShow = $('.canegories-p .sanatorium-card.active-show').length;
        $('.canegories-p .sanatorium-card').each(function (i, e) {
            if ((i <= (lengsShow + resizer) && (!$(this).hasClass('active-show')))) {
                $(this).addClass('active-show');
            }
            if ((i == lengsAll - 1) && $(this).hasClass('active-show')) {
                lengsAll = $('.canegories-p .sanatorium-card').length;
                lengsShow = $('.canegories-p .sanatorium-card.active-show').length;
            }
        });
        if (lengsShow >= lengsAll) {
            $('.canegories-p .more').addClass('hide');
        }
    });

    $('.categories-card-p .btn-more').on('click', function () {
        var resizer;
        if ($(window).outerWidth() >= 1251) {
            resizer = 3;
        } else if ($(window).outerWidth() <= 1250 && $(window).outerWidth() >= 1026) {
            resizer = 2;
        } else if ($(window).outerWidth() <= 1025 && $(window).outerWidth() >= 721) {
            resizer = 1;
        } else if ($(window).outerWidth() <= 720) {
            resizer = 1
        }
        var lengsAll = $('.categories-card-p .sanatorium-card').length;
        var lengsShow = $('.categories-card-p .sanatorium-card.active-show').length;
        $('.categories-card-p .sanatorium-card').each(function (i, e) {
            if ((i <= (lengsShow + resizer) && (!$(this).hasClass('active-show')))) {
                $(this).addClass('active-show');
            }
            if ((i == lengsAll - 1) && $(this).hasClass('active-show')) {
                lengsAll = $('.categories-card-p .sanatorium-card').length;
                lengsShow = $('.categories-card-p .sanatorium-card.active-show').length;
            }
        });
        if (lengsShow >= lengsAll) {
            $('.categories-card-p .more').addClass('hide');
        }
    });

    $('.tabs-content .btn-more').on('click', function () {
        var resizer = 3;
        var lengsAll = $('.reviews-card').length;
        var lengsShow = $('.reviews-card.active-show').length;
        $('.reviews-card').each(function (i, e) {
            if ((i <= (lengsShow + resizer) && (!$(this).hasClass('active-show')))) {
                $(this).addClass('active-show');
            }
            if ((i == lengsAll - 1) && $(this).hasClass('active-show')) {
                lengsAll = $('.reviews-card').length;
                lengsShow = $('.reviews-card.active-show').length;
            }
        });
        if (lengsShow >= lengsAll) {
            $('.tabs-content .more').addClass('hide');
        }
    });

    $(window).on("resize", function () {
        adaptiv();
    });

    //Mob nav
    $('.open-mob-nav').on('click', function () {
        $('.mob_nav').addClass('active');
        $('main').addClass('active_mob');
        $('.header-container').addClass('active_mob');
        $('body').css('overflow', 'hidden');
        $('.exit-active-mob_nav').on('click', function () {
            $('.mob_nav').removeClass('active');
            $('body').css('overflow', 'visible');
            $('main').removeClass('active_mob');
            $('.header-container').removeClass('active_mob');
        });

        //Click not mob nav
        $(document).mouseup(function (e) {
            var div = $(".mob_nav");
            if (!div.is(e.target)
                && div.has(e.target).length === 0) {
                $('.mob_nav').removeClass('active');
                $('body').css('overflow', 'visible');
                $('main').removeClass('active_mob');
                $('.header-container').removeClass('active_mob');
            }
        });
    });

    //Show price
    $(".show-price a").on("click", function (e) {
        e.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top - 100;
        $('body, html').animate({scrollTop: top}, 1000);
    });

    //Show tabs
    $(".tabs li a").on("click", function (e) {
        e.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top - 100;
        $('body, html').animate({scrollTop: top}, 1000);
    });

    //fixed-nav-tabs
    if ($('ul').hasClass('tabs')) {
        var offset = $('.tabs').offset().top;
        $(window).scroll(function () {
            if ($(window).scrollTop() >= offset) {
                $('.tabs').addClass('active');
            } else {
                $('.tabs').removeClass('active');
            }
        });
    }

    //Drop Find List
    $('.find-more-inf').on('keyup', function () {
        var lengs = $(this).val().length;
        if (lengs >= 3) {
            $(this).parent().addClass('active');
        } else {
            $(this).parent().removeClass('active');
        }
        $(this).parent().find('.drop-list a').on('click', function () {
            var clickText = $(this).text();
            $(this).closest('.group').find('.find-more-inf').val(clickText);
            $('.group').removeClass('active');
        });
    });

    $('.group.raiting').on('click', function () {
        $(this).toggleClass('active');

        $('.raiting .drop-list li').on('click', function () {
            var howStar = $(this).html();
            $('.rat-head').html(howStar);
        });
    });

    //Date Select
    $('.date-select').on('click', function () {
        $(this).find('.arr-ic-down').addClass('active');
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $(this).find('input').focusout();
        } else if (!$(this).hasClass('active')) {
            $(this).find('input').trigger("focus");
        }

        $('.date-select input').on('change focusout' , function () {
            $(this).parent().find('.arr-ic-down').removeClass('active');
        });

        $(this).find('.arr-ic-down.active').on('click', function () {
            $(this).parent().find('input').focusout();
        });
    });

    $('.more-tabs span').on('click', function () {
        $(this).parent().find('.drop-tabs').toggleClass('active');
        $('.drop-tabs li a').on('click', function () {
            $('.drop-tabs').removeClass('active');
        })
    });

});







